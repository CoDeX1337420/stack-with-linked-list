#include <iostream>
#include "stack.h"

int flag = 0;
/*
The function get the head element and starts it with null.

input: the pointer to the first element
output: NONE
*/
void initStack(stack* s)
{
    s->list = new stack;
    s->list->value = 0;
    s->list->size = 0;
    s->list->next = 0;
    s->list->list = 0;
    flag = 1;
}

/*
The function gets the staring node of the stach and the element
that that we want to add to stack and adds the element to the top
of the stack.

input: First element of the stack and the element that we watn to place in the stack.
output: NONE
*/
void push(stack* s, unsigned int element)
{
  if (s == NULL)
  {
    std::cout<<"Initialise the stack first..."<<'\n';
  }
  else
  {
    if (flag == 1)
    {
      stack* newNode = new stack;
      newNode->value = element;
      s->list = newNode;
      s->size = s->size + 1;
    }
    else
    {
      stack* newNode = new stack;
      newNode->value = element;
      newNode->next = s->list;
      s->list = newNode;
      s->size = s->size + 1;
    }
  }
  flag = 0;
}

/*
The function gets the first element of the stack and removes the first element
from the stack.

input: the first element of the Stack
output: -1 if the stack is empty and if element removed, the first value that is in the stack.
*/
int pop(stack* s)
{
  if(s->list==NULL)
  {
    std::cout<<"Stack is empty..."<<'\n';
    return -1;
  }
  s->list = s->list->next;

  return s->list->value;
}

/*
The function is made to clean all the stack and to make clean it from numbers.

input: the first element of the stack.
output: NONE
*/
void cleanStack(stack* s)
{
  stack* current = s->list;
  stack* next;

  while (current != NULL)
  {
    next = current->next;
    delete[] current;
    current = next;
  }
}
