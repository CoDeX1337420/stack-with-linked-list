#include "linkedList.h"
#include <iostream>

list* head = NULL;

/*
The function adds new elemnet and puts there the Number.

input: number to place in stack.
output: NONE
*/
void push(int num)
{
    list* newNode = new list;
    newNode->value = num;
    newNode->next = head;
    head = newNode;
}

/*
The function removes the first element in the stack.

input: NONE
output: NONE...
*/
void pop()
{
  if(head==NULL)
  {
    std::cout<<"Stack is empty..."<<'\n';
    return;
  }
  head = head->next;
}
