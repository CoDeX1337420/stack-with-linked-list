#include <iostream>
#include "utils.h"

/*
The function is made to reverse a stack that is made out from ARRAY and not struct,
because in the function was written INT* and not STRUCT STACK*.

input: the pointer to the stack of numbers, its size.
output: NONE
*/
void reverse(int* nums, unsigned int size)
{
  int i = 0;
  int temp = 0;

  int *start = nums;
  int *end = nums + (size - 1);

  for (i = 0; i < size; i++)
  {
    if (start < end)
    {
      temp = *end;
      *end = *start;
      *start = temp;
    }
    start++;
    end--;
  }
}

/*
The function gets 10 numbers from user, calls the reversive function and return the array with the reversed numbers.
*/
int* reverse10()
{
  int i = 0;
  int* userArr = new int[10];
  std::cout << "Enter 10 numbers please: " << '\n';

  for (i = 0; i < 10; i++)
  {
    std::cin >> userArr[i];
  }

  reverse(userArr, 10);

  return userArr;
}
